﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week2
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount account = new BankAccount();

            // passing a value type (decimal) by value which is the default and then by reference
            // and finally by reference using the "out" keyword which requires that the called method update
            // the parameter (total)
            decimal amount = 100;
            account.Deposit(amount);
            account.Deposit(ref amount);
            decimal total;
            account.Deposit(amount, out total);

            // passing a reference type (array) by value which is the default and then by reference
            decimal[] recentTransactions = new decimal[] { 100, -100, 200, -200 };
            account.PostTransactions(recentTransactions);
            account.PostTransactions(ref recentTransactions);
        }
    }
}
